<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mixpanel;

class LibraryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    private $book;
	private $mp;

	public function __construct (Book $book)
    {
		//$this->middleware('auth');
        $this->book = $book;
		// get the Mixpanel class instance, replace with your
		// project token
		$this->mp = Mixpanel::getInstance("9e6ff4d646864845a4fbd315e42768c1");
    }

    public function index()
	{
        $books = $this->book->get();
        //$books = DB::table('books')->get();
        //$books = Book::all();
		// track an event
		$mp = $this->mp;
		$mp->track("Library Index");
		return view('library.home', compact('books'));
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::guest())
		{
			return redirect('library');
		}
		return view('library.create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @internal param $book
     */
	public function store(Request $request)
	{
        //Book::create($request->input());
        $book = new Book();
        $book->fill( $request->input())->save();

		// track an event
		$mp = $this->mp;
		// identify the current request as user 12345
		$mp->identify($book->id);
		$mp->track("New Book Inserted", array("title" => $book->title, "author" => $book->author));

        return redirect('library');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$book = $this->book->find($id);



		// track an event MixPanel
		$mp = $this->mp;
		// identify the current request as user 12345
		$mp->identify($book->id);

		// track an event associated to user 12345
		// track an event
		$mp->track("Book Viewed", array("label" => $book->title));

        //$book = Book::find($id);
        return view('library.show', compact('book'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $book = $this->book->find($id)->delete();
        //Book::destroy($id);
        //test destroy
        return redirect('library');
	}

}