<?php namespace App\Transformers;

use App\Model\Book;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class BookTransformer extends TransformerAbstract
{
	/**
	 * List of resources possible to include
	 *
	 * @var  array
	 */
	protected $availableIncludes = [];

	/**
	 * List of resources to automatically include
	 *
	 * @var  array
	 */
	protected $defaultIncludes = [];


	/**
	 * @param Book $book
	 * @return array
     */
	public function transform(Book $book)
	{
		return [
			'id'     => (int) $book->id,
			'title' => $book->title,
			'published' => (int) $book->published,
			'genre'  => $book->genre
		];
		//return new \League\Fractal\Resource\Collection($resource,new ResourceTransformer);
	}
}

?>
