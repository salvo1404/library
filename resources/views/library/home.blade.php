@extends('master')

@section('content')
    <h2>Welcome to the library</h2>
    <h1><a href="library/create">Insert New Book</a> </h1>
    @foreach($books as $book)
        <li><a href="library/{{$book->id}}">{{ $book->title }}</a>
            <a href="library/destroy/{{$book->id}}">Delete</a> </li>
    @endforeach
@endsection