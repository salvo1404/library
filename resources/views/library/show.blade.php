@extends('master')

@section('content')

    <h2>{{$book->title}}</h2>
    @if($book->published)
        <h1>{{$book->published}}</h1>
    @endif
    <h1>{{$book->author}}</h1>
    <h1>{{$book->genre}}</h1>
    <?php
    //  tracking an event GA
    Analytics::trackEvent('Show Book', $book->title);
    ?>
@endsection