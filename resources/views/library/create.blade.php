@extends('master')

@section('content')
    <h2>Insert a new Book</h2>

    {!! Form::open(['url' => 'library/store', 'id' => 'insert_book']) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('published', 'Published:') !!}
            {!! Form::text('published', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('author', 'Author:') !!}
            {!! Form::text('author', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('genre', 'Genre:') !!}
            {!! Form::text('genre', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Save Book', ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}




@endsection